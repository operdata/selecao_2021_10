**PROBLEMA**

O problema está relacionado a uma marca que possui diversas franquias distribuídas pelo Brasil. Foram implementados métodos inovadores em algumas franquias e a marca gostaria de identificar se a inovação apresentou melhoria nas franquias em relação aquelas que não tiveram intervenção. 

Precisamos selecionar um grupo controle de um conjunto de franquias por meio de análise de cluster a partir de quatro variáveis (idade da franquia, UF, população e quantidade de clientes). Temos um base de dados com resultados ao longo de três meses.  

Precisamos verificar se o grupo teste cresceu mais ou menos no período analisado de acordo com o controle. 
